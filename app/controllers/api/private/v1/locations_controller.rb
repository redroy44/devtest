class Api::Private::V1::LocationsController < ApplicationController
  def index
    locations = Location.filter(params[:country_code], params[:panel_provider_code])
    render json: locations
  end
end

