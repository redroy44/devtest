class Api::Private::V1::TargetGroupsController < ApplicationController
  def index
    target_groups = TargetGroup.filter(params[:country_code], params[:panel_provider_code])
    render json: target_groups
  end
end
