class TargetGroup < ApplicationRecord
  belongs_to :panel_provider
  belongs_to :parent, class_name: 'TargetGroup', foreign_key: :parent_id
  has_and_belongs_to_many :countries
  belongs_to :children, class_name: 'TargetGroup', foreign_key: :parent_id

  validates :name, presence: true

  def self.filter(country_code, panel_provider_code)
    target_groups = TargetGroup
                    .joins(:countries)
                    .joins(:panel_provider)
                    .where(
                      countries: { country_code: country_code},
                      panel_providers: { code: panel_provider_code}
                    )

    target_groups
  end

  def generate_tree(num_children, depth, current_level = 0)
    return if current_level == depth
    num_children.times do |i|
      target_group_name = "#{self.name}-#{i}"
      target_group = TargetGroup.create(
        name: target_group_name,
        external_id: 200 + i,
        secret_code: "super_secret_code_#{i}",
        panel_provider: self.try(:panel_provider),
        parent: self
      )
      target_group.generate_tree(num_children, depth, current_level+1)
    end
  end
end
