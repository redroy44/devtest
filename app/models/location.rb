class Location < ApplicationRecord
  has_and_belongs_to_many :location_groups

  validates :name, presence: true

  def self.filter(country_code, panel_provider_code)
    locations = Location
                .joins(location_groups: :country)
                .joins(location_groups: :panel_provider)
                .where(
                  countries: { country_code: country_code },
                  panel_providers: { code: panel_provider_code }
                )
                .distinct
    locations
  end
end
