Rails.application.routes.draw do
  api_version(:module => "Api::Private::V1", :path => {:value => "api_private/v1"}) do
    get 'locations/:country_code' => 'locations#index'
    get 'target_groups/:country_code' => 'target_groups#index'
    post 'evaluate_target' => 'evaluation#evaluate_target'
  end

  api_version(:module => "Api::Public::V1", :path => {:value => "api/v1"}) do
    get 'locations/:country_code' => 'locations#index'
    get 'target_groups/:country_code' => 'target_groups#index'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
