module PanelProviders
  module Calculators
    class BaseCalculator
      def process(data)
        raise NotImplementedError
      end
    end
  end
end