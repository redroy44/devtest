module PanelProviders
  module Calculators
    class LetterCalculator < BaseCalculator
      def process(data)
        data.count('a') / 100
      end
    end
  end
end