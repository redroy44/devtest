module PanelProviders
  module Calculators
    class NodeCalculator < BaseCalculator
      def process(data)
        html = Nokogiri::HTML(data)
        html.xpath("//*").count / 100
      end
    end
  end
end