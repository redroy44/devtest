module PanelProviders
  module DataFetchers
    class BaseFetcher
      def fetch
        raise NotImplementedError
      end
    end
  end
end