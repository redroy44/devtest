require 'net/http'

module PanelProviders
  module DataFetchers
    class WebFetcher < BaseFetcher
      def fetch(url)
        Net::HTTP.get_response(URI.parse(url)).body
      end
    end
  end
end
