require 'test_helper'

class Api::Private::V1::TargetGroupsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_private_v1_target_groups_index_url
    assert_response :success
  end

end
