require 'test_helper'

class Api::Private::V1::EvaluationControllerTest < ActionDispatch::IntegrationTest
  test "should get evaluate_target" do
    get api_private_v1_evaluation_evaluate_target_url
    assert_response :success
  end

end
