require 'test_helper'

class Api::Public::V1::TargetGroupsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_public_v1_target_groups_index_url
    assert_response :success
  end

end
