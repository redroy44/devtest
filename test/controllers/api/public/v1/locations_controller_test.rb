require 'test_helper'

class Api::Public::V1::LocationsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get api_public_v1_locations_index_url
    assert_response :success
  end

end
