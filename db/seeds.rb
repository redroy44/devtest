# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
#

# PanelProviders & countries
country_codes =  %w(pl es fr)
panel_providers = []
countries = []
3.times do |i|
  panel_provider = PanelProvider.create(code: "Panel#{i+1}")
  country = Country.create(country_code: country_codes[i], panel_provider: panel_provider)

  panel_providers[i] = panel_provider
  countries[i] = country
end

# LocationGroups
location_groups = []
4.times do |i|
  location_group = LocationGroup.create(
    name: "LocationGroup#{i+1}",
    panel_provider: panel_providers[i%3],
    country: countries[i%3]
  )
  location_groups[i] = location_group
end

# Locations
20.times do |i|
  location = Location.create(
    name: "Location#{i}",
    external_id: 100 + i,
    secret_code: "super_secret_code_#{i}"
  )

  (Random.rand(200) % 5).times do |_j|
    location.location_groups << location_groups.sample
  end
end

# TargetGroups
4.times do |i|
  target_group_name = "TargetGroup#{i}"
  target_group_root = TargetGroup.create(
    name: target_group_name,
    external_id: 200 + i,
    secret_code: "super_secret_code_#{i}",
    panel_provider: panel_providers[i%3],
    parent_id: nil,
  )
  (Random.rand(200) % 5).times do |_j|
    target_group_root.countries << countries.sample
  end

  target_group_root.generate_tree(2, 3)
end




