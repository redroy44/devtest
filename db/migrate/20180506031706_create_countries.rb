class CreateCountries < ActiveRecord::Migration[5.0]
  def change
    create_table :countries do |t|
      t.string :country_code
      t.references :panel_provider, index: true, foreign_key: true

      t.timestamps
    end
  end
end
