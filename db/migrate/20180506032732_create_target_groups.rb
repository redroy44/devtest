class CreateTargetGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :target_groups do |t|
      t.string :name
      t.string :external_id
      t.string :secret_code
      t.references :panel_provider, index: true, foreign_key: true
      t.references :parent, references: :target_group, index: true, foreign_key: true

      t.timestamps
    end
  end
end
