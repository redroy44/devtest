class CreateLocationGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :location_groups do |t|
      t.string :name
      t.references :country, index: true, foreign_key: true
      t.references :panel_provider, index: true, foreign_key: true

      t.timestamps
    end
  end
end
